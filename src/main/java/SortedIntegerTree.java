public class SortedIntegerTree {
    private Entry rootEntry;

    public void add(int newValue) {
        Entry newEntry = new Entry(newValue);
        if (rootEntry == null) {
            rootEntry = newEntry;
            return;
        }
        addNewEntry(newEntry, rootEntry);
    }

    public boolean exist(int value) {
        return entryExist(value, rootEntry);
    }

    private boolean entryExist(int value, Entry currentEntry) {
        if (currentEntry == null) {
            return false;
        }
        if (currentEntry.getValue() == value) {
            return true;
        }
        Entry nextEntry;
        if (value > currentEntry.getValue()) {
            nextEntry = currentEntry.getRight();
        } else {
            nextEntry = currentEntry.getLeft();
        }
        return entryExist(value, nextEntry);
    }

    private void addNewEntry(Entry newEntry, Entry currentEntry) {
        if (newEntry.getValue() > currentEntry.getValue()) {
            if (currentEntry.getRight() == null) {
                currentEntry.setRight(newEntry);
            } else {
                addNewEntry(newEntry, currentEntry.getRight());
            }
        } else {
            if (currentEntry.getLeft() == null) {
                currentEntry.setLeft(newEntry);
            } else {
                addNewEntry(newEntry, currentEntry.getLeft());
            }
        }
    }
}

class Entry {
    private int value;
    private Entry left;
    private Entry right;

    public Entry(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Entry getLeft() {
        return left;
    }

    public void setLeft(Entry left) {
        this.left = left;
    }

    public Entry getRight() {
        return right;
    }

    public void setRight(Entry right) {
        this.right = right;
    }
}